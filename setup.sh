#!/bin/bash

USER=${USER:-$(id -u -n)}
HOME="${HOME:-$(getent passwd $USER 2>/dev/null | cut -d: -f6)}"
HOME="${HOME:-$(eval echo ~$USER)}"

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

sudo apt -y update
sudo apt -y upgrade
sudo apt -y install htop wget curl git zabbix-agent2

